//
//  TCScene.m
//  TCProgressBarNode
//
//  Created by Charles Chamblee on 7/23/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCScene.h"
#import "TCProgressBarNode.h"

#define kCyclesPerSecond 0.15f

@interface TCScene ()

@property (nonatomic, strong) TCProgressBarNode *progressBarNode;

@property (nonatomic) NSTimeInterval startTime;

@end

@implementation TCScene

#pragma mark - Init / Dealloc

- (id)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

#pragma mark - Scene Lifecycle

- (void)didMoveToView:(SKView *)view
{
    [super didMoveToView:view];
    
    self.size = view.bounds.size;
    
    [self configureProgressBarNode];
}

#pragma mark - Configuration

- (void)configureProgressBarNode
{
    self.progressBarNode = [[TCProgressBarNode alloc] initWithSize:CGSizeMake(300.0, 32.0)
                                                   backgroundColor:[UIColor darkGrayColor]
                                                         fillColor:[UIColor colorWithRed:2.0/255.0 green:124.0/255.0 blue:34.0/255.0 alpha:1.0]
                                                       borderColor:[UIColor lightGrayColor]
                                                       borderWidth:2.0
                                                      cornerRadius:4.0];
    
    self.progressBarNode.titleLabelNode.text = @"TCProgressBarNode";
    
    self.progressBarNode.position = CGPointMake(round(self.size.width / 2.0), round(self.size.height / 2.0f));
    
    [self.scene addChild:self.progressBarNode];
}

- (void)update:(NSTimeInterval)currentTime
{
    [super update:currentTime];
    
    CGFloat secondsElapsed = currentTime - self.startTime;
    CGFloat cycle = secondsElapsed * kCyclesPerSecond;
    CGFloat progress = cycle - (NSInteger)cycle;
    
    self.progressBarNode.progress = progress;
}

@end
