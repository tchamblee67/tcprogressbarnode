//
//  TCViewController.m
//  TCProgressBarNode
//
//  Created by Charles Chamblee on 7/23/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCViewController.h"
#import "TCScene.h"

@import SpriteKit;

@interface TCViewController ()

@property (nonatomic, strong) SKView *spriteKitView;
@property (nonatomic, strong) TCScene *scene;

@end

@implementation TCViewController

#pragma mark - View Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureSpriteKitView];
    [self configureScene];
}

#pragma mark - Configuration

- (void)configureSpriteKitView
{
    self.spriteKitView = [[SKView alloc] initWithFrame:self.view.bounds];
    
    [self.view insertSubview:self.spriteKitView atIndex:0];
}

- (void)configureScene
{
    self.scene = [TCScene sceneWithSize:self.spriteKitView.bounds.size];
    
    self.scene.scaleMode = SKSceneScaleModeResizeFill;
    
    [self.spriteKitView presentScene:self.scene];
}

#pragma mark - IBActions

- (IBAction)didTapBlog:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tonychamblee.com"]];
}

@end
