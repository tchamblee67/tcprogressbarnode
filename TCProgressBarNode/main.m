//
//  main.m
//  TCProgressBarNode
//
//  Created by Charles Chamblee on 7/23/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
